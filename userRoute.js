//import dependencies

const express = require("express");
const router = express.Router(); //Create Route
const userController = require("../controllers/user")


//Route to check for duplicate emails
router.post("/checkEmail", (req,res) => {
	userController.checkEmail(req.body).then(resultFromController => res.send(
			resultFromController))

})

//Route for registration
router.post("/register", (req, res) => {
	console.log(req)
	userController.registerUser(req.body).then(resultFromController => res.send(
			resultFromController))
	//userController.registerUSer is to call registerUser in user file
	// Then you pass information using req.body
	// .then wait for true or false, or resultFromController will return true or false
})


// Route for login 
router.post("/login", (req, res) =>{
	userController.loginUser(req.body).then(resultFromController => res.send(
		resultFromController
		))
})






module.exports = router; 
